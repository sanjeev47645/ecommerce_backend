const STATUS_CODE = {
    ERROR: 0,
    SUCCESS: 1
}
// const twilio = {
//     ACCSID: 'ACeccf40d3225ef72cb853a14cd56d4167',
//     AUTHTKN: '52d8d467091e43f3caf5b78ae08d9832',
//     PHN_NO: '+13342343931'
// }

// const twilio = {
//     ACCSID: 'AC7c47b0f835493be8bf622acfd2cbcf79',
//     AUTHTKN: '4232062cadc03913432d891724b1585e',
//     PHN_NO: '+18188692490'
// }

// const MSG91 = {
//     AUTH_KEY: '288802Af2I7KMwMzZ5d4c033d'
// }

const DB_MODEL_REF = {
    USER: "User",
    SETTING: 'settings',
    ADMIN: 'admin',
    CAT: "categories",
    PRODUCT: "products",
    ORDERS: "orders",
    INVNTRY: "inventries"
}

// ========================== Export Module Start ==========================
// const MESSAGES = {
//     INVAILD_CRD: 'Invaild credentials'
// }
const DEVICE_TYPE = {
    ANDROID: 1,
    IOS: 2
}


const VAILDATEMESSAGES = {
    keyCantEmpty: "{{key}} can not be empty",
    invalidEmail: "Email is invalid"
}


module.exports = Object.freeze({
    APP_NAME: 'ecommerce',
    STATUS_CODE,
    DB_MODEL_REF,
    // MESSAGES,
    // twilio,
    DEVICE_TYPE,
    // MSG91,
    VAILDATEMESSAGES
});
// ========================== Export Module End ============================