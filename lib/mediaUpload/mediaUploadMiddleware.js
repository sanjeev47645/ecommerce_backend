/**
 * Created by vikas kohli
 */
var Promise = require("bluebird");
path = require('path'),
    fs = require('fs'),
    _ = require('lodash'),
    unlink = Promise.promisify(fs.unlink, fs),
    logger = require('../logger/index').logger;
var config = require(".././config").cfg;

var customExc = require("./../customException");

var {
    isObjEmp
} = require('./../appUtils');

var uploadDeleteToS3 = require('../services/uploadDeleteToS3');

// var seq = new Date().getTime();
function getfileKey(chk, name) {
    let fileKey;
    switch (chk) {
        case 'usr_img':
            fileKey = config.s3UploadPaths.user.profile;
            break;
        case 'prod_imgs':
            fileKey = config.s3UploadPaths.product.img;
            break;
        case 'cat_img':
            fileKey = config.s3UploadPaths.cat.img;
            break;
        default:
            fileKey = config.s3UploadPaths.user.profile;
            break;
    }
    return fileKey + name;
}

module.exports = {
    uploadSingleMediaToS3: function s3SingleFileUpload(chk, multikey = false) {
        return function (req, res, next) {
            var file = (req.files || req.file);
            // console.log("uploadSingleMediaToS3", file);
            if (!file) {
                return next();
            }
            if (!isObjEmp(file)) {
                return next();
            }
            if (multikey) {
                if (file[chk])
                    file = file[chk][0];
                else
                    return next()
            }
            let fileKey = getfileKey(chk, file.filename)
            return new Promise(function (resolve, reject) {
                return uploadDeleteToS3.uploadFile(file, fileKey)
                    .then(function (url) {
                        // console.log('url', url);
                        return next();
                    })
                    .catch(function (err) {
                        console.log(err)
                        // console.log("Error :", err);
                        return next(customExc.completeCustomException("intrnlSrvrErr", false));
                        // throw err;
                    })
            })
        }
    },



    uploadMultipleMediaToS3: function s3MultipleFileUpload(chk) {

        return function (req, res, next) {
            console.log(req.files);
            // console.log(req.body.filesArray, chk, 'chk');
            var files = req.files;
            if (!files) {
                return next();
            }
            Promise.mapSeries(files, file => {
                let fileKey = getfileKey(chk, file.filename)
                return uploadDeleteToS3.uploadFile(file, fileKey);
            })
                .then(urls => {
                    return next();
                })
                .catch(err => {
                    return next(customExc.completeCustomException("intrnlSrvrErr", false));
                })
        }
    }



}

function _fetchFilesFromReq(req) {

    if (req.file) {
        return [req.file];
    } else if (req.files) {
        return req.files;
    } else {
        //No Data
        return [];
    }
}

function _fetchMultipleFilesFromReq(req, keys) {

    if (req.file) {
        return [req.file];
    } else if (req.files) {
        var filesArr = [];
        keys.forEach(function (key) {
            if (req.files[key]) {
                filesArr.push(req.files[key][0]);
            }
        })
        return filesArr;
    } else {
        //No Data
    }
}

function __deleteFiles(filePathList) {
    var promiseArray = [];

    _.each(_.uniq(filePathList), function (path) {
        promiseArray.push(unlink(path))
    })

    Promise.all(promiseArray)
        .then(() => logger.info(TAG, "All Files Deleted Successfully"))
        .catch(err => logger.error(err))
}