class BaseDao {
    constructor(dbModel) {
        //Get Model
        this.Model = dbModel;
    }

    save(object) {
        return this.Model.create(object);
    }

    findOne(query, projection) {
        return this.Model.findOne(query, projection).lean().exec();
    }

    insertMany(arr) {
        return this.Model.insertMany(arr);
    }

    find(query, projection, pagination = {}) {
        return this.Model.find(query, projection, pagination);
    }

    findOneAndUpdate(query, update, options) {
        return this.Model.findOneAndUpdate(query, update, options).exec();
    }

    findAndModify(query, update, options) {
        return this.Model.findAndModify(query, update, options).exec();
    }

    findByIdAndUpdate(query, update, options) {
        return this.Model.findByIdAndUpdate(query, update, options).exec();
    }

    findById(query, projections) {
        return this.Model.findById(query, projections).lean().exec();
    }
    /**
     * Update Given Model
     * @param query
     * @param toUpdate
     * @return Promise Object
     * @private
     */
    update(query, update, options) {
        if (!options) {
            options = {};
        }
        return this.Model.update(query, update, options).exec();
    }

    updateMany(query, update, options = {}) {
        return this.Model.updateMany(query, update);
    }

    remove(query, options) {
        return this.Model.remove(query, options).exec();
    }

    findByIdAndRemove(query, options) {
        return this.Model.findByIdAndRemove(query, options).exec();
    }
    aggregate(aggPipe) {
        return this.Model.aggregate(aggPipe).exec();
    }
    popFind(query, popRef, projection = {}) {
        return this.Model.find(query, projection).populate(popRef).exec();
    }
    popFindOne(query, popRef, projection = {}, popOption = null) {
        return this.Model.findOne(query, projection).populate(popRef, popOption).exec()
    }

}

// ========================== Export Module Start ==========================
module.exports = BaseDao;
// ========================== Export Module End ============================