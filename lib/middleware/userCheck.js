"use strict";
//========================== Load Modules Start ===========================
var Promise = require("bluebird");
//========================== Load internal Module =========================
const jwtHandler = require("../jwtHandler");
const constants = require("../constant");
const customExceptions = require("../customException");
const redisClient = require("../redisClient/init");
const userDao = require("../modules/user/userDao")


var isUserExist = async function (req, res, next) {
    let response = await userDao.getByKey({ email: req.body.email });
    if (response) {
            next();
    } else {
         next(customExceptions.completeCustomException('usr_n_exst'));
    }
}
module.exports = {
    isUserExist
};