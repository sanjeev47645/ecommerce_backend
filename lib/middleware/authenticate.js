"use strict";
//========================== Load Modules Start ===========================

//========================== Load internal Module =========================
var exceptions = require("../customException.js");
var jwtHandler = require("../jwtHandler");
const userDao = require('./../modules/user/userDao');
const admnDao = require('./../modules/admin/adminDao');

//========================== Load Modules End =============================

var __verifyTok = acsTokn =>
    jwtHandler.verifyToken(acsTokn)
        .then(tokenPayload => {
            return tokenPayload
        })
        .catch(err => {
            err.msg = err.message
            delete err.message;
            throw err
        });
var expireToken = (req, res, next) =>
    jwtHandler
        .expireToken(req)
        .then(result => {
            //return result;
            next();
        })
        .catch(err => {
            next(err);
        });
var usrAutntctTkn = async (req, res, next) => {
    var acsToken = req.get("accessToken");
    if (!!acsToken) {
        try {
            let vrfyTkn = await __verifyTok(acsToken);
            let response = await userDao.getByKey({ _id: vrfyTkn.user_id });
            if (response) {
                req.user = vrfyTkn;
                next();
            } else {
                throw exceptions.completeCustomException('usr_n_exst')
            }
            // __verifyTok(acsToken)
            //     .then(tokenPayload => {
            //         return tokenPayload
            //         // req.user = tokenPayload;
            //         // next();
            //     })
            //     .then(function (result) {vrfyTkn
            // return userDao.getByKey({_id:result._id}) 
            //     .then(res => {
            //         if (res) {
            //             req.user = result;
            //             next()
            //         } else {
            //             throw exceptions.completeCustomException('usr_n_exst')
            //         }
            //     });
            // })
        } catch (err) {
            next(err);
        }
    } else {
        throw exceptions.unauthorizeAccess()
    }
};
const adminAutntctTkn = async (req, res, next) => {
    var acsToken = req.get("accessToken");
    if (!!acsToken) {
        let vrfyTkn = await __verifyTok(acsToken);
        let response = await admnDao.getByKey({ _id: vrfyTkn.user_id });
        if (response) {
            req.user = vrfyTkn;
            next();
        } else
            throw exceptions.completeCustomException('admn_n_exst');
    } else
        throw exceptions.unauthorizeAccess()
}
var verifyClientSecreate = (req, res, next) => {
    //  req.body=JSON.parse(req.body);
    var clientSecret = req.body.clientSecret;

    console.log("jsonData...", clientSecret);
    if (clientSecret != "Mi6lhaR10HyWOxjMqITx3ONWHFkTcHuebIZPYNi1") {
        throw exceptions.invalidClientSecreate();
    }
    next();
};

//========================== Export Module Start ===========================

module.exports = {
    usrAutntctTkn,
    expireToken,
    verifyClientSecreate,
    adminAutntctTkn
};

//========================== Export Module End ===========================