const multer = require('multer'),
	fs = require('fs'),
	config = require('../config');

// var init = function () {
// 	if (!fs.existsSync(config.cfg.uploadDir)) {
// 		fs.mkdir(config.cfg.uploadDir, 0744);
// 	}
// }
var fileName;
var storage = multer.diskStorage({
	destination: function (request, file, callback) {
		callback(null, config.cfg.uploadDir);
	},
	filename: function (request, file, callback) {
		var time = new Date().getTime();
		fileName = file.fieldname + '_' + time + '_' + file.originalname.slice(file.originalname.lastIndexOf('.') - 1);
		callback(null, fileName);
	}
});
var upload = multer({
	storage: storage
});

function _singleFile(key) {
	return upload.single(key);
}

function _fileArray(key, count) {
	return upload.array(key, count);
}

const fields = arr => {
	return upload.fields(arr);
}

// ========================== Export Module Start ==========================
module.exports = {
	single: _singleFile,
	array: _fileArray,
	fields
}
// ========================== Export Module End ============================