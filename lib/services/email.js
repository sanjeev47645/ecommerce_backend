var nodemailer = require("nodemailer");
const config = require("../config").cfg;
var handlebars = require("handlebars");
const Promise = require('bluebird')
var fs = require("fs");

var readHTMLFile = function (path, callback) {
    fs.readFile(
        path, {
            encoding: "utf-8"
        },
        function (err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        }
    );
};
let smtpTransport = nodemailer.createTransport({
    service: config.smtp.host,
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: config.smtp.auth,
    tls: {
        rejectUnauthorized: false
    }
});
const sendMail = payload => {
    var fromEmail = config.smtp.auth.user;
    var toEmail = payload.to;
    var subject = payload.subject;
    readHTMLFile(__dirname + `/emailTemplate/${payload.template}`, function (
        err,
        html
    ) {
        var template = handlebars.compile(html);
        var data = payload.data;
        var htmlToSend = template(data);
        var mailOptions = {
            from: fromEmail,
            to: toEmail,
            subject: subject,
            html: htmlToSend
        };
        return new Promise((resolve, reject) => {
            smtpTransport.sendMail(mailOptions, (err, response) => {
                if (err) {
                    return reject(err)
                }
                return resolve(response);
            });
        })
    });
};

module.exports = {
    sendMail
};