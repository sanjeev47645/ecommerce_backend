const twilio = require("twilio");
const twConst = require("./../constant");
var client = new twilio(twConst.twilio.ACCSID, twConst.twilio.AUTHTKN);

//send msg via twilio
module.exports = {
    send_msg: (number, otp) => {
        return client.messages
            .create({
                body: `Your confidential Login OTP is ${otp}`,
                to: number, // Text this number
                from: twConst.twilio.PHN_NO // From a valid Twilio number
            })
            .then(message => {
                    console.log(message.sid)
                    return {
                        status: true
                    }
                }
                //message.sid ? true : false
            ).catch(err => {
                return {
                    status: false,
                    err
                }
            })
    }
};