// const msg91 = require('msg91-sms');
// const constant = require('./../constant');

// module.exports = {
//     send_msg: (number, msg, code, ) => {
//         let authkey = constant.MSG91.AUTH_KEY, senderid = 611332, route = 4
//         //message = `Your confidential Login OTP is ${otp}`;
//         try {
//             msg91.sendOne(authkey, number, msg, senderid, route, code, res => {
//                 console.log(res, 'dgdf');
//                 //Returns Message ID, If Sent Successfully or the appropriate Error Message
//                 if (res)
//                     return { status: true }
//             });
//         } catch (err) {
//             return { status: false, err }
//         }
//     }
// }

const msg91 = require('msg91-promise');
const constant = require('./../constant');


module.exports = {
    send_msg: (number, msg) => {
        let authkey = constant.MSG91.AUTH_KEY, senderid = 'TRPARK', route = 4
        //message = `Your confidential Login OTP is ${otp}`;
        var msg91Sms = msg91(authkey, senderid, route);
        return msg91Sms.send(number, msg).then(res => {
            return { status: true };
        }).catch(err => {
            return { status: false, err }
        })
    }
}