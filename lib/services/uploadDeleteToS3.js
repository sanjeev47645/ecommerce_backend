"use strict";

//========================== Load Modules Start =======================

//========================== Load External modules ====================
const Promise = require("bluebird");
const AWS = require('aws-sdk');
const fs = require("fs");
const path = require("path");
// const sharp = require('sharp');
// var customExc = require('./../customException');

//========================== Load internal modules ====================
var config = require("../config").cfg;
AWS.config = {
    accessKeyId: config.awsIamUser.accessKeyId,
    secretAccessKey: config.awsIamUser.secretAccessKey,
    region: config.s3.region,
    bucketName: config.s3.bucketName,
    // signatureVersion: config.s3.signatureVersion
};
// console.log(AWS.config);
var Bucket = config.s3.bucketName;
var photoBucket = new AWS.S3({
    credentials: AWS.config,
    params: {
        Bucket
    }
});

//========================== Load Modules End ==============================================
function __deleteTempFile(filePath) {
    fs.stat(filePath, function (err, stats) {
        //console.log(stats);//here we got all information of file in stats variable
        if (err) {
            console.error(err);
        }
        fs.unlink(filePath, function (err) {
            if (err) {
                console.log(err);
            }
            console.log('file deleted successfully');
        });
    });
};

function __uploadToS3(file, buffer, fileKey) {
    return new Promise(function (resolve, reject) {
        photoBucket.upload({
            Key: fileKey,
            ContentType: file.mimetype || 'image/png',
            Body: buffer,
            ACL: 'public-read'
        }, function (err, data) {
            if (err) {
                //console.log("upload fail: ",err);
                //Also delete file if error occurs
                __deleteTempFile(file.path);
                reject(err);
            } else {
                //Adding file name in data, so save only filename
                data.filename = file.filename;
                data.fieldname = file.fieldname;
                resolve(data);
            }
        });

    })
}


async function uploadFile(file, fileKey) {
    var buffer = await __buffer(file);
    return __uploadToS3(file, buffer, fileKey)
        .then(function (data) {
            console.log("then calling");
            __deleteTempFile(file.path);
            return data
        }).catch(function (err) {
            __deleteTempFile(file.path);
            throw err;
        });
    // }
}

const __buffer = file => {
    var buffer;
    switch (file.mimetype) {
        // case 'image/jpeg':
        //     buffer = sharp(file.path).jpeg({ quality: 10 }).toBuffer();
        //     break;
        // case 'image/png':
        //     buffer = sharp(file.path).jpeg({ quality: 5 }).toBuffer();
        //     break;
        // case 'image/jpg':
        //     buffer = sharp(file.path).jpg({ quality: 10 }).toBuffer();
        //     break;
        default:
            buffer = fs.createReadStream(file.path);
    }
    return buffer
}

function uploadImageThumb(file) {
    let size = 128;
    let dest = path.join(file.path, "../");
    let resizeName = size + "x" + size + file.filename;
    dest += resizeName;
    return new Promise(function (resolve, reject) {

        gm(file.path)
            .resize(size, size)
            .autoOrient()
            .write(dest, function (err) {
                if (err) {
                    reject(err);
                }
                let buffer = fs.createReadStream(dest);
                let resizeImage = file;
                resizeImage.filename = resizeName;
                // resizeImage.path = dest;
                // resizeImage;
                return __uploadToS3(resizeImage, buffer)
                    .then(function (data) {
                        __deleteTempFile(dest);
                        resolve(data);
                    })
                    .catch(function (err) {
                        throw err
                    })
            });
    });
}



// function deleteFromS3(fileKey, file){
//     console.log("fileDelting", fileKey, file.filename);

function deleteFromS3(fileKey) {
    console.log("fileDelting", fileKey);
    photoBucket.deleteObject({
        Key: fileKey
    }, function (err, data) {
        if (err) {
            console.log("delete fail: ", err, fileKey);
            return false
        } else {
            console.log("delete success: ", data, fileKey);
            return true
        }
    });
    return true;
}

/*function rotateMediaWithFFMPEF(file) {
 let filePath = path.join(file.path, "../" + "rotated" + file.filename);
 return new Promise(function (resolve, reject) {
 gm(file.path).identify(function (err, metadata) {
 let angle = 0;
 if (metadata.Orientation.toLowerCase() == "bottomright") {
 angle = appConst.ORIENTATION.bottomright;
 }
 if (metadata.Orientation.toLowerCase() == "lefttop") {
 angle = appConst.ORIENTATION.lefttop;
 }
 if (metadata.Orientation.toLowerCase() == "righttop") {
 angle = appConst.ORIENTATION.righttop;
 }
 if (metadata.Orientation.toLowerCase() == "rightbottom") {
 angle = appConst.ORIENTATION.rightbottom;
 }
 if (metadata.Orientation.toLowerCase() == "leftbottom") {
 angle = appConst.ORIENTATION.leftbottom;
 }
 if (angle) {
 var execCmd = "ffmpeg -i " + file.path + " -vf 'transpose=" + angle / 90 + "' -strict -2 " + filePath;
 exec(execCmd, function (err, response) {
 __deleteTempFile(file.path);
 if (err) {
 reject(err);
 }
 file.path = filePath;
 resolve(file);
 })
 } else {
 //file.path = filePath;
 resolve();
 }
 })
 });
 }*/



//========================== Export Module Start ==============================

module.exports = {
    uploadFile,
    uploadImageThumb,
    __deleteTempFile,
    deleteFromS3
};

//========================== Export Module End ===============================