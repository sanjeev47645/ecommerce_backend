const ios = require('./ios/ios'),
    android = require('./android/android'),
    constant = require('../../constant');

function sendNotification(registrationIds, data) {
    let promiseResult = [];

    for (var i = 0; i < registrationIds.length; i++) {
        if (registrationIds[i].dev_type == constant.DEVICE_TYPE.ANDROID && !!registrationIds[i].dev_tkn) {
            promiseResult.push(android.pushNotification(registrationIds[i].dev_tkn, data));
        }
        if (registrationIds[i].dev_type == constant.DEVICE_TYPE.IOS && !!registrationIds[i].dev_tkn) {
            promiseResult.push(ios.pushNotification(registrationIds[i].dev_tkn, data));
        }
    }
    return Promise.all(promiseResult);
}

const singleNotfn = (regId, data) => {
    if (regId.dev_type == constant.DEVICE_TYPE.ANDROID) {
        return android.pushNotification(regId.dev_tkn, data);

    }
    if (regId.dev_type == constant.DEVICE_TYPE.IOS) {
        return ios.pushNotification(regId.dev_tkn, data);
    }
    return false;
}

// ========================== Export Module Start ==========================
module.exports = {
    sendNotification,
    singleNotfn
}
// ========================== Export Module End ============================