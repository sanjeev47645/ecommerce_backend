class Exception {
	constructor(errorCode, message, errorStackTrace, userType) {
		this.errCode = errorCode;
		this.msg = message;
		// this.errorMessage = message;
		if (errorStackTrace) {
			this.err = errorStackTrace;
		}
	}
}

// ========================== Export Module Start ==========================
module.exports = Exception;
// ========================== Export Module End ============================