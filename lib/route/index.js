const basicAuth = require('../middleware/basicAuth'),
    cat = require('./../modules/categories/catRoute'),
    usrRoutr = require('../modules/user/userRoute'),
    admnRoutr = require('../modules/admin/adminRoute'),
    productRoutr = require('../modules/product/productRoute'),
    invntryRouter = require('./../modules/inventory/invntryRoute'),
    responseHandler = require('../responseHandler');

//========================== Export Module Start ==========================
console.log(__dirname);
module.exports = function (app) {
    app.use(basicAuth.basicAuthentication);

    // Attach User Routes
    app.use('/api/v2/user', usrRoutr);
    app.use('/api/v2/cat', cat);
    app.use('/api/v2/product', productRoutr);
    app.use('/api/v2/admin', admnRoutr);
    app.use('/api/v2/invntry', invntryRouter);
    // Attach ErrorHandler to Handle All Errors
    app.use(responseHandler.handleError);
}
//========================== Export Module End ============================