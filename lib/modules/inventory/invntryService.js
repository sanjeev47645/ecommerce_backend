"use strict";

//========================== Load Modules Start =======================

//========================== Load internal modules ====================

// Load product dao
const invntryDao = require('./invntryDao');
const { regexIncase } = require('../../appUtils');
//========================== Export Module Start ==============================


const __listing_filter = info => {
    let query = {};
    if (info.srch)
        query.name = regexIncase(info.srch)
    return query;
}


module.exports = {};

//========================== Export Module End ===============================