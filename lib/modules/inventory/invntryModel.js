// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../constant");

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    usr_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    prdct_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

//Export user module
module.exports = mongoose.model(constants.DB_MODEL_REF.INVNTRY, UserSchema);
/*module.exports = rootRef*/