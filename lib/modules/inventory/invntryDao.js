"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
//========================== Load internal modules ====================
const Invntry = require('./invntryModel');



// init user dao
let BaseDao = new require('../../dao/baseDao');
const invntryDao = new BaseDao(Invntry);
//init settings dao

//========================== Load Modules End ==============================================

const insert = info => {
    let invntry = new Invntry(info);
    return invntryDao.save(invntry);
}
const getByKey = (query, project = {}) => invntryDao.findOne(query, project);

const getById = (query, project = {}) => invntryDao.findById(query, project);

const getMany = (query, project = {}, paginate = {}) => invntryDao.find(query, project, paginate);

const updateByKey = (query, update, options = {}) => invntryDao.findOneAndUpdate(query, update, options)

const updateById = (query, update, options = {}) => invntryDao.findByIdAndUpdate(query, update, options);

const updateMany = (query, update, options = {}) => invntryDao.updateMany(query, update, options);

const agg = (pipe = []) => invntryDao.aggregate(pipe);
const delById = (query, options = {}) => invntryDao.findByIdAndRemove(query, options);


//========================== Export Module Start ==============================

module.exports = {
    insert,
    updateById,
    getById,
    getByKey,
    agg,
    updateByKey,
    getMany,
    delById,
    updateMany
};

//========================== Export Module End ===============================