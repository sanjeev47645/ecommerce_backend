//========================== Load Modules Start ===========================

//========================== Load external Module =========================
var _ = require("lodash");

//========================== Load Internal Module =========================
var appUtils = require("../../appUtils");
var constant = require("./../../constant");
var customExc = require("../../customException");

//========================== Load Modules End =============================



//========================== Export Module Start ===========================


const login = (req, res, next) => {
    var { email, pass } = req.body;
    var { } = req.headers;
    let obj = {}
    if (!email) {
        obj.key = "email"
        errObj(obj)
    }
    if (!appUtils.isValidEmail(email)) {
        obj.key = "email"
        obj.msg = constant.VAILDATEMESSAGES.invalidEmail
        errObj(obj)
    }
    if (!pass) {
        obj.key = "pass"
        errObj(obj)
    }
    next();
}

const resetPass = (req, res, next) => {
    var { new_pass, old_pass } = req.body;
    var { } = req.headers;
    let obj = {}
    if (!new_pass) {
        obj.key = "new_pass"
        errObj(obj)
    }
    if (!old_pass) {
        obj.key = "old_pass"
        errObj(obj)
    }
    next();
}
const forgotPass = (req, res, next) => {
    var { email } = req.body;
    var { } = req.headers;
    let obj = {}
    if (!email) {
        obj.key = "email"
        errObj(obj)
    }
    if (!appUtils.isValidEmail(email)) {
        obj.key = "email"
        obj.msg = constant.VAILDATEMESSAGES.invalidEmail
        errObj(obj)
    }
    next();
}
const changePass = (req, res, next) => {
    var { pass, tkn } = req.body;

    if (!pass) {
        obj.key = "pass"
        errObj(obj)
    }
    if (!tkn) {
        obj.key = "tkn"
        errObj(obj)
    }
    next();
}

const errObj = ({ key, msg = false }) => {
    let error = {
        fieldName: key,
        message: constant.VAILDATEMESSAGES.keyCantEmpty.replace("{{key}}", key)
    }
    if (msg)
        error.message = `${msg}`
    throw customExc.validationErrors(error)
}

module.exports = {
    login,
    changePass,
    forgotPass,
    resetPass
};
//========================== Export module end ==================================