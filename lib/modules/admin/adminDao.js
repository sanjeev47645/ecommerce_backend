"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
//========================== Load internal modules ====================
const Admin = require('./adminModel');



// init user dao
let BaseDao = new require('../../dao/baseDao');
const admnDao = new BaseDao(Admin);
//init settings dao

//========================== Load Modules End ==============================================

const signUp = userInfo => {
    userInfo.img = userInfo.usr_img
    let user = new Admin(userInfo);
    return admnDao.save(user);
}

const getByKey = (query, project = {}) => admnDao.findOne(query, project);

const getById = (query, project = {}) => admnDao.findById(query, project);

const getMany = (query, project = {}, paginate = {}) => admnDao.find(query, project, paginate);

const updateByKey = (query, update, options = {}) => admnDao.findOneAndUpdate(query, update, options)

const updateById = (query, update, options = {}) => admnDao.findByIdAndUpdate(query, update, options);

const updateMany = (query, update, options = {}) => admnDao.updateMany(query, update, options);

const agg = (pipe = []) => admnDao.aggregate(pipe);
const delById = (query, options = {}) => admnDao.findByIdAndRemove(query, options);


//========================== Export Module Start ==============================

module.exports = {
    signUp,
    updateById,
    getById,
    getByKey,
    agg,
    updateByKey,
    getMany,
    delById,
    updateMany
};

//========================== Export Module End ===============================