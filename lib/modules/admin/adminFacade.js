"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
//========================== Load internal modules ====================
// Load user service
const adminService = require("./adminService");
const adminMapper = require("./adminMapper");
const jwtHandler = require("../../jwtHandler");
const appUtils = require("../../appUtils");
const redisClient = require("../../redisClient/init");
const custExc = require("../../customException");
const email = require("./../../services/email");
const config = require('./../../config').cfg;

//========================== Load Modules End ==============================================

/**
 * @function login
 * login via phone no
 * @param {Object} loginInfo login details
 */
const login = loginInfo => {
    return adminService
        .login(loginInfo)
        .then(admin => {
            if (admin) {
                let hash = appUtils.encryptHashPassword(
                    loginInfo.pass,
                    admin.salt
                );
                if (hash == admin.hash) {
                    this.user = admin;
                    var tokenObj = buildUserTokenGenObj(admin);
                    return jwtHandler.generateAdminToken(tokenObj);
                } else {
                    throw custExc.completeCustomException(
                        "pas_incr",
                        false
                    );
                }
            } else {
                throw custExc.completeCustomException("admn_n_exst", false);
            }
        })
        .then(jwt => {
            this.jwt = jwt;
            let redisObj = appUtils.createRedisValueObject({
                user: this.user
            });
            redisClient.setValue(jwt, JSON.stringify(redisObj));
            return adminMapper.loginMapping({
                user: this.user,
                jwt
            });
        })
        .catch(err => {
            console.log(err);
            throw err;
        });
}

const changepass = info => {
    if (info.user_id) {
        return adminService
            .admin_exist(info)
            .then(user => {
                if (user) {
                    let hash = appUtils.encryptHashPassword(
                        info.old_pass,
                        user.salt
                    );
                    if (hash == user.hash) {
                        info.update = appUtils.generateSaltAndHashForPassword(
                            info.new_pass
                        );
                        return adminService.updateResetPass(info);
                    } else {
                        throw custExc.completeCustomException(
                            "old_pss_incr",
                            false
                        );
                    }
                } else {
                    throw custExc.unauthorizeAccess();
                }
            })
            .then(update => {
                if (update) return adminMapper.resetpassMapping();
            })
            .catch(err => {
                throw err;
            });
    } else
        throw custExc.unauthorizeAccess();
};
const forgotPass = info => {
    var tkn = appUtils.genRandStr(10);
    info.tkn = tkn;
    return adminService
        .updateToken(info)
        .then(setTkn => {
            if (setTkn) {
                var link = config.path.frgt_pss_admn + appUtils.base64En(tkn)
                let payload = {
                    to: info.email,
                    template: "forgotPass.html",
                    subject: "forgot pass link",
                    data: {
                        link,
                        base_url: config.BASE_URL.URL
                    }
                };
                email.sendMail(payload);
                return adminMapper.frgtPass();
            } else throw custExc.completeCustomException("admn_n_exst", false);
        })
        .catch(err => {
            throw err;
        });
};
const resetPass = info => {
    info.tkn = appUtils.base64De(info.tkn);
    return adminService.getByTkn(info)
        .then(admin => {
            if (admin) {
                let within_day = appUtils.withinDay(admin.frgt_pass.time);
                if (within_day) {
                    info.id = admin._id;
                    info.update = appUtils.generateSaltAndHashForPassword(
                        info.pass
                    );
                    info.update.frgt_pass = {};
                    return adminService.upFrgtPass(info);
                } else throw custExc.completeCustomException("link_exp", false);
            } else {
                throw custExc.completeCustomException("lnk_incrt", false);
            }
        })
        .then(update => {
            if (update) return adminMapper.resetpassMapping();
        })
        .catch(err => {
            throw err;
        });
};


function buildUserTokenGenObj(user) {
    var userObj = {};
    userObj.user_id = user._id;
    return userObj;
}

//========================== Export Module Start ==============================

module.exports = {
    login,
    resetPass,
    forgotPass,
    changepass,
};

//========================== Export Module End ===============================signUp