const admnRoutr = require("express").Router();
const resHndlr = require("../../responseHandler");
const admnFacade = require("./adminFacade");
const validators = require("./adminValidators");
const middleware = require("./../../middleware");

admnRoutr.route("/login").post([validators.login], (req, res) => {
    let info = { email, pass } = req.body;
    admnFacade.login(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
});
admnRoutr.route("/change/pass").put([middleware.authenticate.adminAutntctTkn, validators.resetPass], (req, res) => {
    let info = {
        old_pass,
        new_pass
    } = req.body;
    info.user_id = req.user.user_id
    admnFacade.changepass(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req)
    })
})
admnRoutr.route('/forgot/pass').post([validators.forgotPass], (req, res) => {
    let info = {
        email: req.body.email
    }
    admnFacade.forgotPass(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})
admnRoutr.route('/reset/pass').post([validators.changePass], (req, res) => {
    let info = {
        pass: req.body.pass,
        tkn: req.body.tkn
    };
    admnFacade.resetPass(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req)
    })
})




module.exports = admnRoutr;