// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../constant");

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String,
        default: ""
    },
    email: {
        type: String,
        index: true,
        required: true,
        unique: true
    },
    hash: String,
    salt: String,
    frgt_pass: {
        tkn: String,
        time: { type: Date }
    },
    img: {
        type: String,
        default: ""
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

//Export user module
module.exports = mongoose.model(constants.DB_MODEL_REF.ADMIN, UserSchema);
/*module.exports = rootRef*/