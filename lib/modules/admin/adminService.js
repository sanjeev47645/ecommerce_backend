"use strict";

//========================== Load Modules Start =======================

//========================== Load internal modules ====================

// Load user dao
const adminDao = require('./adminDao');

//========================== Load Modules End ==============================================




const login = loginInfo => {
    let query = {};
    query.email = loginInfo.email;
    return adminDao.getByKey(query);
}

const admin_exist = info => {
    let query = {
        _id: info.user_id
    };
    return adminDao.getById(query);
}

const updateResetPass = info => {
    let query = {
        _id: info.user_id
    },
        update = info.update;
    return adminDao.updateById(query, update);
}
//update 
const updateToken = info => {
    let query = {
        email: info.email
    },
        update = {
            "frgt_pass.tkn": info.tkn,
            "frgt_pass.time": new Date()
        };
    return adminDao.updateByKey(query, update);
}

const upFrgtPass = info => {
    let query = {
        _id: info.id
    },
        update = info.update;
    return adminDao.updateById(query, update);
}

const getByTkn = info => {
    let query = {
        "frgt_pass.tkn": info.tkn
    }
    return adminDao.getByKey(query);
}


//========================== Export Module Start ==============================

module.exports = {
    login,
    admin_exist,
    updateResetPass,
    updateToken,
    getByTkn,
    upFrgtPass
};

//========================== Export Module End ===============================