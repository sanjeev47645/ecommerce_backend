/**
 * This file will have request and response object mappings.
 */

const status_code = require('./../../status_codes');


const loginMapping = params => {
    var respObj = {
        "acsTkn": params.jwt,
        "admPrfl": {
            "_id": params.user._id,
            "name": params.user.name,
            "email": params.user.email,
        }
    }
    return respObj;
}

const resetpassMapping = () => {
    let resObj = {
        ...status_code.rst_pss
    }
    return resObj;
}
const frgtPass = () => {
    let resObj = {
        ...status_code.forgot_mail_sent
    }
    return resObj;
}

module.exports = {
    loginMapping,
    resetpassMapping,
    frgtPass
}