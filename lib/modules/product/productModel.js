// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../constant");

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String,
        index: true,
        default: ""
    },
    meta_title: {
        type: String,
        index: true
    },
    cat_id: {
        type: Schema.Types.ObjectId,
        index: true,
        required: true
    },
    desc: { type: String },
    shrt_desc: String,
    sku: String,
    price: { type: Number, required: true },
    spl_price: Number,
    quantity: Number,
    tax: {
        type: String,
        enum: ["0", "1", "2"]//0=>no taxes,1=>taxes,2 =>shiping
    },
    status: {
        type: Boolean,
        default: true
    },
    is_del: {
        type: Boolean,
        default: false
    },
    url_key: String,
    visiblity: {
        type: Boolean,
        default: true
    },
    in_stock: {
        type: Number,
    },
    details: String,
    imgs: [{ type: String }],
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

//Export user module
module.exports = mongoose.model(constants.DB_MODEL_REF.PRODUCT, UserSchema);
/*module.exports = rootRef*/