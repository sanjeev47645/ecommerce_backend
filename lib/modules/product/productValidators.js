//========================== Load Modules Start ===========================

//========================== Load external Module =========================
var _ = require("lodash");

//========================== Load Internal Module =========================
var appUtils = require("../../appUtils");
var constant = require("./../../constant");
var customExc = require("../../customException");

//========================== Load Modules End =============================



//========================== Export Module Start ===========================
var obj = {};
const add = (req, res, next) => {
    let { name, price } = req.body;
    if (!name) {
        obj.key = "name"
        errObj(obj);
    }
    if (!price) {
        obj.key = "price";
        errObj(obj)
    }
    next();
}


const errObj = ({ key, msg = false }) => {
    let error = {
        fieldName: key,
        message: constant.VAILDATEMESSAGES.keyCantEmpty.replace("{{key}}", key)
    }
    if (msg)
        error.message = `${msg}`
    throw customExc.validationErrors(error)
}

module.exports = { add };
//========================== Export module end ==================================