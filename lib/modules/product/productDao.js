"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
//========================== Load internal modules ====================
const Product = require('./productModel');



// init user dao
let BaseDao = new require('../../dao/baseDao');
const productDao = new BaseDao(Product);
//init settings dao

//========================== Load Modules End ==============================================

const insert = info => {
    let product = new Product(info);
    return productDao.save(product);
}
const getByKey = (query, project = {}) => productDao.findOne(query, project);

const getById = (query, project = {}) => productDao.findById(query, project);

const getMany = (query, project = {}, paginate = {}) => productDao.find(query, project, paginate);

const updateByKey = (query, update, options = {}) => productDao.findOneAndUpdate(query, update, options)

const updateById = (query, update, options = {}) => productDao.findByIdAndUpdate(query, update, options);

const updateMany = (query, update, options = {}) => productDao.updateMany(query, update, options);

const agg = (pipe = []) => productDao.aggregate(pipe);
const delById = (query, options = {}) => productDao.findByIdAndRemove(query, options);


//========================== Export Module Start ==============================

module.exports = {
    insert,
    updateById,
    getById,
    getByKey,
    agg,
    updateByKey,
    getMany,
    delById,
    updateMany
};

//========================== Export Module End ===============================