const productRoutr = require("express").Router();
const resHndlr = require("../../responseHandler");
const productFacade = require("./productFacade");
const validators = require("./productValidators");
const middleware = require("./../../middleware");
const mediaUpload = require("../../mediaUpload/mediaUploadMiddleware");

productRoutr.route('/add').post([middleware.authenticate.adminAutntctTkn, middleware.multer.array('prod_imgs'), mediaUpload.uploadMultipleMediaToS3('prod_imgs'), validators.add], (req, res) => {
    let info = req.body;
    info.imgs = req.files ? req.files.map(y => y.filename) : []
    productFacade.addProduct(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})
productRoutr.route('/admin/listing').get([middleware.authenticate.adminAutntctTkn], (req, res) => {
    let info = req.query;
    productFacade.adminList(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})
productRoutr.route('/delete/:product_id').delete([middleware.authenticate.adminAutntctTkn], (req, res) => {
    let info = req.params;
    productFacade.delProduct(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })

})
productRoutr.route('/update/:product_id').put([middleware.authenticate.adminAutntctTkn], (req, res) => {
    let info = req.params;
    productFacade.editProduct(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})


module.exports = productRoutr;