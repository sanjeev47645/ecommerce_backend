"use strict";

//========================== Load Modules Start =======================

//========================== Load internal modules ====================

// Load product dao
const productDao = require('./productDao');
const { regexIncase } = require('./../../appUtils');
//========================== Export Module Start ==============================

const addProduct = info => productDao.insert(info)

const adminList = info => {
    let query = __listing_filter(info),
        limit = 10, skip = 0, project = {
            name: 1, meta_title: 1, sku: 1,
            price: 1, quantity: 1, in_stock: 1, is_del: 1, imgs: 1
        };
    if (info.limit && !isNaN(info.limit))
        limit = Number(info.limit);
    if (info.page && !isNaN(info.page))
        skip = Number(limit * info.page)
    let paginate = { limit, skip };
    return productDao.getMany(query, project, paginate);
}
const totalProduct = info => {
    let query = __listing_filter(info);
    return productDao.getMany(query);
}
const delProduct = info => {
    let query = { _id: info.product_id }, update = { is_del: true };
    return productDao.updateById(query, update)
}
const __listing_filter = info => {
    let query = {};
    if (info.srch)
        query.name = regexIncase(info.srch)
    return query;
}

const editProduct = info => {
    let query = { _id: info.product_id }, update = info.update;
    return productDao.updateById(query, update);
}

module.exports = {
    addProduct,
    adminList,
    totalProduct,
    delProduct,
    editProduct
};

//========================== Export Module End ===============================