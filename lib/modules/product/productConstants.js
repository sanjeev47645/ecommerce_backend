module.exports = {
    MESSAGES: {
        keyCantEmpty: "{{key}} can not be empty",
        invalidEmail: "Email is invalid"
    }
}