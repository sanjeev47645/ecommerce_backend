"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
//========================== Load internal modules ====================
// Load user service
const productService = require("./productService");
const productMapper = require("./productMapper");
const jwtHandler = require("../../jwtHandler");
const appUtils = require("../../appUtils");
const custExc = require("../../customException");
const config = require('./../../config').cfg;
const status_codes = require('./../../status_codes')

//========================== Load Modules End ==============================================

/**
 * @function addProduct
 * login via phone no
 * @param {Object} info add details
 */
const addProduct = info => {
    return productService.addProduct(info).then(product => {
        if (product)
            return { ...status_codes.prd_ad }
        else
            throw custExc.completeCustomException('prd_nt_ad')
    }).catch(err => {
        throw err
    })
}

/**
 * @function adminList
 * login via phone no
 * @param {Object} info filteration data
 */

const adminList = info => {
    return productService.totalProduct(info).count().then(total => {
        this.total = total
        let list = productService.adminList(info)
        if (info.srt_dir && !!info.srt_val) {
            switch (info.srt_val) {
                case "name":
                    list.sort({ name: info.srt_dir })
                case "price":
                    list.sort({ price: info.srt_dir })
            }
        }
        return list
    }).then(products => {
        let bp = config.s3UploadPaths.endPoint + config.s3UploadPaths.product.img
        products.imgs = products.length ? products[0] : '';
        return { products, total: this.total, bp }
    }).catch(err => {
        throw err;
    })
}
const delProduct = info => {
    return productService.delProduct(info).then(deletedProduct => {
        if (!deletedProduct.is_del) {
            return { ...status_codes.prdct_del }
        } else
            throw custExc.completeCustomException('prdct_alrdy_del')
    })
}


const editProduct = info => {
    return productService.editProduct(info).then(upProduct => {
        if (upProduct) return { ...status_codes.prdct_up }
    }).catch(err => {
        throw err
    })
}
//========================== Export Module Start ==============================

module.exports = {
    addProduct,
    adminList,
    delProduct,
    editProduct
};

//========================== Export Module End ===============================signUp