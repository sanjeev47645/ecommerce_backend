"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
//========================== Load internal modules ====================
const User = require('./userModel');



// init user dao
let BaseDao = new require('../../dao/baseDao');
const userDao = new BaseDao(User);
//init settings dao
var {
    isObjEmp
} = require('./../../appUtils');

//========================== Load Modules End ==============================================

const insert = userInfo => {
    let user = new User(userInfo);
    return userDao.save(user);
}



const getByKey = (query, project = {}) => userDao.findOne(query, project);

const get = (query, project = {}) => userDao.findById(query, project);

const getMany = (query, project = {}, paginate = {}) => userDao.find(query, project, paginate);

const updateByKey = (query, update, options = {}) => userDao.findOneAndUpdate(query, update, options)

const update = (query, update, options = {}) => userDao.findByIdAndUpdate(query, update, options);

const updateMany = (query, update, options = {}) => userDao.updateMany(query, update, options);

const agg = (pipe = []) => userDao.aggregate(pipe);
const delById = (query, options = {}) => userDao.findByIdAndRemove(query, options);


//========================== Export Module Start ==============================

module.exports = {
    insert,
    update,
    get,
    getByKey,
    agg,
    updateByKey,
    getMany,
    delById,
    updateMany
};

//========================== Export Module End ===============================