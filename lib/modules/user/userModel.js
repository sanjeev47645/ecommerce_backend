// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../constant");

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
        type: String,
        default: ""
    },
    phone: {
        type: String,
        index: true,
    },
    gender: {
        type: String,
        index: true,
    },
    hash: String,
    salt: String,
    code: {
        type: String,
    },
    email: {
        type: String,
        lowercase: true,
        trim: true,
    },
    auth_id: {
        type: String,
        index: true
    },
    type: {
        type: String,
        default: "EM",
        enum: ["EM", 'FB', 'G']
    },
    cntry: {
        type: String,
    },
    loc: {
        type: [Number],
        index: "2dsphere"
    },
    devdta: [{
        devid: {
            type: String,
            default: ""
        },
        devtkn: {
            type: String,
            default: ""
        },
        devtype: {
            type: Number,
            default: 0 //1=>android,2=>ios
        }
    }],
    address: [{
        name: {
            type: String,
            default: ""
        },
        phone: {
            type: String,
            default: ""
        },
        pincode: {
            type: String,
            default: ''
        },
        locality: {
            type: String,
            default: ""
        },
        address: {
            type: String,
            default: ""
        },
        city: {
            type: String,
            default: ""
        },
        state: {
            type: String,
            default: ""
        },
        landmark: {
            type: String,
            default: ""
        },
        altphone: {
            type: String,
            default: ""  // alternate phone
        },
        type: {
            type: Number,
            default: 1  // 1=> Home , 2=> Office
        },
    }],
    cart: [
        {
            prdct_id: {
                type: Schema.Types.ObjectId,
                required: true
            },
            qty: {
                type: Number,
                required: true
            }
        }
    ],
    card: [{
        nmbr: {
            type: String,
            default: ""  // alternate phone
        },
        name: {
            type: String,
            default: ''
        },
        mnth: {
            type: Number,
            default: ""  // alternate phone
        },
        year: {
            type: Number,
            default: ""  // alternate phone
        },
        altname: {
            type: Number,
            default: 1  // 1=> Home , 2=> Office
        },
    }],
    notfn: {
        type: Boolean,
        default: true
    },
    status: {
        type: Boolean,  // true => active, false=> inactive
        default: true
    },
    // acs_tkn: {
    //     type: String,
    //     default: ""
    // },
    created: {
        type: Date,
        default: new Date()
    },
    updated: {
        type: Date,
        default: new Date()
    }
});

//Export user module
module.exports = mongoose.model(constants.DB_MODEL_REF.USER, UserSchema);
/*module.exports = rootRef*/