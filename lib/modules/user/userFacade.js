"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
//========================== Load internal modules ====================
// Load user service
const usrService = require('./userService');
const userMapper = require('./userMapper');
//const twlio = require('./../../services/twilio');
const customExc = require('./../../customException');
const msg91 = require('./../../services/msg91');
const email = require("./../../services/email");

const redisClient = require("../../redisClient/init");
const jwtHandler = require('../../jwtHandler');
const appUtils = require('../../appUtils');
const deltos3 = require("./../../services/uploadDeleteToS3")
const cnfg = require('./../../config').cfg;
const pushNotf = require('./../../services/pushNotification/notificationService');

//========================== Load Modules End ==============================================


const login = info => {
    if (!info.pwd) {
        return Promise.resolve({
            "code": 16,
            "msg": "User exist proceed to login"
        });
    }
    switch (info.type) {
        case "EM":
            return email_login(info);
        case "FB" || "G":
            return social_login(info)
        default:
            throw { msg: "please select login type" }
    }
}

const email_login = info => {
    return usrService.updtTkn(info).then(user => {
        return usrService.login(info).then(user => {
            this.user = user;
            if (user) {
                let hash = appUtils.encryptHashPassword(info.pwd, user.salt);
                if (hash == user.hash) {
                    var tokenObj = buildUserTokenGenObj(user);
                    return jwtHandler.generateAdminToken(tokenObj);
                } else
                    throw customExc.completeCustomException('pas_incr')
            }
        }).then(jwt => {
            this.jwt = jwt;
            let redisObj = appUtils.createRedisValueObject({
                user: this.user
            });
            redisClient.setValue(jwt, JSON.stringify(redisObj));
            this.user.acsTkn = jwt;
            this.user.msg = 'login successfuly';
            return userMapper.signupMapping(this.user, jwt);
        })
    }).catch(err => {
        console.log(err);
        throw err;
    })
}
const social_login = info => {
    return usrService.updtTkn(info).then(user => {
        return usrService.social_exist(info).then(result => {
            console.log(result);
            if (result) {
                return result;
            } else {
                return usrService.signUp(info);
            }
        }).then(user => {
            this.user = user;
            var tokenObj = buildUserTokenGenObj(user);
            console.log(tokenObj);
            return jwtHandler.generateAdminToken(tokenObj);
        }).then(jwt => {
            this.jwt = jwt;
            // let redisObj = appUtils.createRedisValueObject({
            //     user: this.user
            // });
            // redisClient.setValue(jwt, JSON.stringify(redisObj));
            this.user.acsTkn = jwt;
            this.user.msg = 'login successfuly'
            return this.user;
            return userMapper.signupMapping(this.user, jwt);
        })
    }).catch(err => {
        console.log(err);
        throw err;
    })

}
const buildUserTokenGenObj = user => {
    var userObj = {};
    userObj.user_id = user._id;
    userObj.email = user.email;
    userObj.name = user.name;
    return userObj;
}



const create = async info => {
    try {
            let response = await usrService.login({ email: info.email });
            let resphn = await usrService.login({ phone: info.phone });
            if (response)
                throw customExc.completeCustomException('usr_exst');
            else if (resphn)
                throw customExc.completeCustomException('phn_exst');
            else {
                let hash = appUtils.generateSaltAndHashForPassword(info.pwd);
                let response = await usrService.signUp({ ...info, ...hash });
                if (response) {
                    var tokenObj = buildUserTokenGenObj(response);
                    let jwt = await jwtHandler.generateAdminToken(tokenObj);
                    let ifdevtknext = await usrService.updtTkn(info);
                    let redisObj = appUtils.createRedisValueObject({
                        user: tokenObj
                    });
                    redisClient.setValue(jwt, JSON.stringify(redisObj));
                    return userMapper.signupMapping(response, jwt);
                }
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
    }


const address = async info => {
        try {
            let query = {};
            query._id = info.user_id;
            let address = {
                $push: {
                    address: { ...info }
                }
            };
            let response = await usrService.address(query, address);
            if (response) {
                return userMapper.addressadd(response);
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
    }


    const updateaddress = async info => {
        try {
            let query = {};
            query._id = info.user_id;
            let address = {
                $push: {
                    address: { ...info }
                }
            };
            let response = await usrService.address(query, address);
            if (response) {
                return userMapper.addressadd(response);
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
    }





    //========================== Export Module Start ==============================

    module.exports = {
        login, create, address, updateaddress
    };

//========================== Export Module End ===============================signUp