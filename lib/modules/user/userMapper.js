/**
 * This file will have request and response object mappings.
 */
const config = require('../../config').cfg; 
var status_codes = require("../../status_codes");

const signupMapping = (params, jwt) => {
    let resObj = {
        ...status_codes.signup_succ,
        "acsTkn": jwt,
        _id: params._id,
        name: params.name,
        img: !!params.img ? config.s3UploadPaths.endPoint + config.s3UploadPaths.user.profile + params.img : ''
    }
    return resObj;
}

const addressadd = (params) => {
    let resObj = {
        ...status_codes.add_address_succ
    }
    return resObj;
}
const addressupdt = (params) => {
    let resObj = {
        ...status_codes.updt_address_succ
    }
    return resObj;
}
module.exports = {
    signupMapping, addressadd, addressupdt
}