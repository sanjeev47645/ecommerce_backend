//========================== Load Modules Start ===========================

//========================== Load external Module =========================
var _ = require("lodash");

//========================== Load Internal Module =========================
var appUtils = require("../../appUtils");
var userConst = require("./userConstants");
var customExc = require("../../customException");

//========================== Load Modules End =============================



//========================== Export Module Start ===========================


var validateSignup = function (req, res, next) {
    var {email,phone,name,code,cntry,pwd} = req.body;
    var { } = req.headers;
    var errors = [];
    let usrData = {email, phone,name,code,cntry,pwd}
    _.reject(usrData, (value,key )=> {
        // if((key == 'email')){
        //     errors.push({
        //         fieldName: key,
        //         message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", key)
        //     });
        // }
        if(_.isNil(value)){
            errors.push({
                fieldName: key,
                message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", key)
            });
        }
    });
    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    // next();
}
var validateadrs = function (req, res, next) {
    let {
        pincode,phone,name,locality,address,city,state,landmark,type,altphone
    } = req.body;
    var { accessToken} = req.headers;
    var errors = [];
    let usrData = {
        accessToken,
        locality,address,city,state,landmark,type,altphone,phone,pincode,
        name
    }
    _.reject(usrData, (value,key )=> {
        if(_.isNil(value)){
            errors.push({
                fieldName: key,
                message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", key)
            });
        }
    });
    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    // next();
}
var validateLogin = function (req, res, next) {
    var { email, pwd} = req.body;
    var {  dev_tkn, dev_typ} = req.headers;
    var errors = [];
    let data = {
        dev_tkn, dev_typ, email, pwd
    }
    // _.reject(data, (value,key )=> {
    //     if(_.isNil(value)){
    //         errors.push({
    //             fieldName: key,
    //             message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", key)
    //         });
    //     }
    // });
    if (_.isEmpty(email)) {
        errors.push({
            fieldName: "email",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "Email")
        });
    }

    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    next();
};
var validateUpd = (req, res, next) => {
    let update = {};
    if (req.body.name) update.name = req.body.name;
    if (req.body.cntry) update.cntry = req.body.cntry;
    if (req.body.is_visible) update.is_visible = req.body.is_visible;
    if (req.body.notf) update.notfn.notf = req.body.notf;
    if (req.body.stln) update.notfn.stln = req.body.stln;
    if (req.file)
        update.img = req.file.filename;
    if (appUtils.isObjEmp(update)) {
        update.updated = new Date;
        req.update = update;
        next();
    } else {
        throw customExc.completeCustomException('no_update')
    }
}
var validateVerfPass = (req, res, next) => {
    let phone = req.body.phone,
        pass = req.body.pass;
    var errors = [];
    if (_.isEmpty(phone)) {
        errors.push({
            fieldName: "phone",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "phone")
        });
    }

    if (_.isEmpty(pass)) {
        errors.push({
            fieldName: "pass",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "pass")
        });
    }
    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    next()
}
var validateChngPass = (req, res, next) => {
    let old_pass = req.body.old_pass,
        new_pass = req.body.new_pass;
    var errors = [];
    if (_.isEmpty(old_pass)) {
        errors.push({
            fieldName: "old_pass",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "old_pass")
        });
    }

    if (_.isEmpty(new_pass)) {
        errors.push({
            fieldName: "new_pass",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "new_pass")
        });
    }
    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    next()
}
var validatemailSent = function (req, res, next) {
    var email = req.body.email;
    var { } = req.headers;
    var errors = [];
    if (!appUtils.isValidEmail(email)) {
        errors.push({
            fieldName: "email",
            message: userConst.MESSAGES.invalidEmail
        });
    }
    if (_.isEmpty(email)) {
        errors.push({
            fieldName: "email",
            message: userConst.MESSAGES.keyCantEmpty.replace("{{key}}", "email")
        });
    }
    if (errors && errors.length > 0) {
        return next(customExc.validationErrors(errors))
    }
    next();
}

module.exports = {
    validateSignup,
    validateLogin,
    validateUpd,
    validateVerfPass,
    validateChngPass,
    validatemailSent,
    validateadrs
};
//========================== Export module end ==================================