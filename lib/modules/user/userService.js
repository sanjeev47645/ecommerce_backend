"use strict";

//========================== Load Modules Start =======================
const mongoose = require('mongoose');
//========================== Load internal modules ====================
const cnst = require('./../../constant');
const userDao = require('./userDao');
const { regexIncase } = require('./../../appUtils')

//========================== Load Modules End ==============================================




const login = info => {
    let query = {};
    query.email = info.email;
    return userDao.getByKey(query)
}

const updtTkn = info => {
    let update = {};
    return userDao.updateByKey({ email: info.email }, { $pull: { 'devdta': { 'devtkn': info.dev_tkn } } })
        .then(res => {
            _.extend(update, {
                $push: {
                    devdta: {
                        devtype: info.dev_type,
                        // devid: info.devid,
                        devtkn: info.dev_tkn,
                    }
                }
            }
            )
            return userDao.updateByKey({ email: info.email }, update, { new: true })
        })
}

const social_exist = info => {
    let query = {};
    query.auth_id = info.auth_id;
    query.type = info.type;
    return userDao.getByKey(query)
}
const signUp = info => userDao.insert(info)
const address = (query, info) => userDao.updateByKey(query, info)
const update_user = info => {
    let query = {},
        update = info.update;
    query._id = info.user_id;
    return userDao.update(query, update)
};





//========================== Export Module Start ==============================

module.exports = {
    login,
    signUp,
    update_user,
    social_exist,
    address,
    updtTkn
};

//========================== Export Module End ===============================