const usrRoutr = require("express").Router();
const resHndlr = require("../../responseHandler");
const middleware = require("../../middleware");
const usrFacade = require("./userFacade");
const validators = require("./userValidators");
const mediaUpload = require("../../mediaUpload/mediaUploadMiddleware");
const cnfg = require('./../../config').cfg;

/********* User Login=> Same will check userexist and if exist will through errorcode 14 else code 15 in middleware
@param User add params
@return user add response 
**********/

usrRoutr.route('/login').post([validators.validateLogin, middleware.userCheck.isUserExist], (req, res) => {
    let info = req.body;
    let { dev_type, dev_tkn } = req.headers
    usrFacade.login({ dev_type, dev_tkn, ...info }).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })

})

/********* User Signup=> user will be signed up with required info then acstkn given to create instant login
@param User add params
@return user add response 
**********/
usrRoutr.route('/create').post([validators.validateSignup], (req, res) => {
    let info = req.body;
    usrFacade.create(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })

})

/********* User Addresses=> user will have multiple addresses for home and office
@param User add params
@return user add response 
**********/
usrRoutr.route('/address').post([validators.validateadrs, middleware.authenticate.usrAutntctTkn], (req, res) => {
    let info = req.body;
    let user = req.user;
    usrFacade.address({ ...info, ...user }).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })

})


usrRoutr.route("/change/pass").put([middleware.authenticate.usrAutntctTkn], (req, res) => {
    let info = {
        old_pass,
        new_pass
    } = req.body;
    info.user_id = req.user.user_id
    admnFacade.changepass(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req)
    })
})
usrRoutr.route('/forgot/pass').post([], (req, res) => {
    let info = {
        email: req.body.email
    }
    admnFacade.forgotPass(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})
usrRoutr.route('/reset/pass').post([], (req, res) => {
    let info = {
        pass: req.body.pass,
        tkn: req.body.tkn
    };
    admnFacade.resetPass(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req)
    })
})

module.exports = usrRoutr;