//========================== Load Modules Start ===========================

//========================== Load external Module =========================
var _ = require("lodash");

//========================== Load Internal Module =========================
var appUtils = require("../../appUtils");
var constant = require("./../../constant");
var customExc = require("../../customException");

//========================== Load Modules End =============================



//========================== Export Module Start ===========================
var obj = {};
const add = (req, res, next) => {
    let { name } = req.body;
    if (!name) {
        obj.key = "name"
        errObj(obj)
    }
    next();
}

const del = (req, res, next) => {
    let { cat_id } = req.params;
    if (!cat_id) {
        obj.key = "cat_id"
        errObj(obj)
    }
    next();
}

const edit = (req, res, next) => {
    let { cat_id } = req.params;
    if (cat_id) {
        obj.key = "cat_id"
        errObj(obj);
    }
    let data = req.body, update = {};
    if (data.name) update.name = data.name;
    if (data.p_id) update.p_id = data.p_id;
    if (data.ancestors) update.ancestors = data.ancestors;
    if (data.meta_title) update.meta_title = data.meta_title;
    if (data.desc) update.desc = data.desc;
    if (data.attr) update.attr = data.attr;
    if (data.order) update.order = data.order;
    if (data.tag) update.tag = data.tag;
    if (data.level) update.level = data.level;
    if (data.status) update.status = data.status;
    if (!appUtils.isObjEmp(update)) {
        update.updated = new Date();
        req.update = update;
        next();
    } else {
        next(customExc.completeCustomException('nothg_upd'));
    }

}
const errObj = ({ key, msg = false }) => {
    let error = {
        fieldName: key,
        message: constant.VAILDATEMESSAGES.keyCantEmpty.replace("{{key}}", key)
    }
    if (msg)
        error.message = `${msg}`
    throw customExc.validationErrors(error)
}

module.exports = {
    add,
    del,
    edit
};
//========================== Export module end ==================================