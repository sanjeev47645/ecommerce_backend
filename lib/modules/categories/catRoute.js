const catRoutr = require("express").Router();
const resHndlr = require("../../responseHandler");
const middleware = require("../../middleware");
const catFacade = require("./catFacade");
const mediaUpload = require("../../mediaUpload/mediaUploadMiddleware");
const vaildators = require('./catValidators');
const cnfg = require('./../../config').cfg;



catRoutr.route('/add').post([middleware.authenticate.adminAutntctTkn,
middleware.multer.single('cat_img'),
mediaUpload.uploadSingleMediaToS3('cat_img'), vaildators.add], (req, res) => {
    let info = { name, p_id, meta_title, desc, attr, order, tag, ancestors, level } = req.body;
    info.img = req.file ? req.file.filename : "";

    catFacade.addCat(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
       // console.log(err, 'gfcfc/');
        resHndlr.sendError(res, err, req)
    })
});

catRoutr.route('/get').get([], (req, res) => {
    let info = {} = req.query;
    catFacade.getCat(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})

catRoutr.route('/dropdown/list').get([middleware.authenticate.adminAutntctTkn], (req, res) => {
    let info = { srch, p_id, ref } = req.query;
    catFacade.dropdownList(info).then(result => {
        resHndlr.sendSuccess(res, result, req);
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})

catRoutr.route('/admin/list').get([middleware.authenticate.adminAutntctTkn], (req, res) => {
    let info = { srch, limit, page, srt_dir, srt_val } = req.query
    catFacade.adminListing(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        console.log(err);
        resHndlr.sendError(res, err, req);
    })
})
catRoutr.route('/admin/:cat_id').delete([middleware.authenticate.adminAutntctTkn], (req, res) => {
    info = { cat_id } = req.params;
    catFacade.deleteCat(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})

catRoutr.route('/update/:cat_id').put([middleware.authenticate.adminAutntctTkn, vaildators.edit], (req, res) => {
    let info = { cat_id } = req.params;
    info.update = req.update;
    catFacade.editCat(info).then(result => {
        resHndlr.sendSuccess(res, result, req)
    }).catch(err => {
        resHndlr.sendError(res, err, req);
    })
})
module.exports = catRoutr;