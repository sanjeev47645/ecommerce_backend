/**
 * This file will have request and response object mappings.
 */
const config = require('../../config').cfg;
var status_codes = require("../../status_codes.json");

const add = () => {
    let resObj = { ...status_codes.ad_cat }
    return resObj;
}

const get = cat => {
    let resObj = { cat }
    return resObj;
}

const getCatList = catList => {
    
}

module.exports = {
    add,
    get,
    getCatList
}