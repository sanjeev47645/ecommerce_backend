"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
//========================== Load internal modules ====================
const Cat = require('./catModel');



// init user dao
let BaseDao = new require('../../dao/baseDao');
const catDao = new BaseDao(Cat);
//init settings dao
var {
    isObjEmp
} = require('./../../appUtils');

//========================== Load Modules End ==============================================

const insert = userInfo => {
    let cat = new Cat(userInfo);
    return catDao.save(cat);
}



const getByKey = (query, project = {}) => catDao.findOne(query, project);

const getById = (query, project = {}) => catDao.findById(query, project);

const getMany = (query, project = {}, paginate = {}) => catDao.find(query, project, paginate);

const updateByKey = (query, update, options = {}) => catDao.findOneAndUpdate(query, update, options)

const updateById = (query, update, options = {}) => catDao.findByIdAndUpdate(query, update, options);

const updateMany = (query, update, options = {}) => catDao.updateMany(query, update, options);

const agg = (pipe = []) => catDao.aggregate(pipe);
const delById = (query, options = {}) => catDao.findByIdAndRemove(query, options);
const delBykey = (query, options = null) => catDao.remove(query, options);


//========================== Export Module Start ==============================

module.exports = {
    insert,
    updateById,
    getById,
    getByKey,
    agg,
    updateByKey,
    getMany,
    delById,
    updateMany,
    delBykey
};

//========================== Export Module End ===============================