"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
//========================== Load internal modules ====================
// Load user service
const catService = require('./catService');
const catMapper = require('./catMapper');
//const twlio = require('./../../services/twilio');
const customExc = require('./../../customException');


const jwtHandler = require('../../jwtHandler');
const appUtils = require('../../appUtils');
const deltos3 = require("./../../services/uploadDeleteToS3")
const cnfg = require('./../../config').cfg;
const status_codes = require('./../../status_codes')

//========================== Load Modules End ==============================================


/**
 * @function addCat 
 * signup user
 * @param {Object} info categories details
 */

const addCat = info => {
    if (info.p_id != null) {
        info = _.extend(info, { 'view_name': info.name });
        info = _.extend(info, { 'parent_cat': [] });
        return getCatData(info)
            .then(data => {
                // info = _.extend(info,{'view_name':data.view_name});
                return catService.addCat(info).then(cat => {
                    return catMapper.add();
                }).catch(err => {
                    throw err;
                })
            });
    } else {
        info = _.extend(info, { 'view_name': info.name });
        return catService.addCat(info).then(cat => {
            return catMapper.add();
        }).catch(err => {
            throw err;
        })
    }
}

/**
 * @function getCat
 * signup user
 * @param {Object} info fetch parameter
 */
const getCat = async info => {
    info.p_id = null;
    var parent = await catService.getCat(info).then(result => result);
    parent = JSON.parse(JSON.stringify(parent));
    for (let i = 0; i < parent.length; i++) {
        info.p_id = parent[i]._id
        var row = await catService.getCat(info).then(result => result);
        row = JSON.parse(JSON.stringify(row));
        for (let j = 0; j < row.length; j++) {
            info.p_id = row[j]._id
            var column = await catService.getCat(info).then(result => result);
            row[j].column = column;
        }
        parent[i].row = row;
    }
    return { cat: parent }
}


/**
 * @function dropdownList 
 * drop down list categories
 * @param {Object} info fetch parameter
 */


const dropdownList = info => {
    return catService.dropdownList(info).then(cat => {
        // let parent = cat.filter(y => !y.p_id);
        // let parent_id = parent.map(y => y._id.toString());
        // let child1 = cat.filter(y => parent_id.includes(!y.p_id ? '' : y.p_id.toString()))
        // cat = parent.concat(child1);
        return { cat }
    }).catch(err => {
        throw err
    })
}
// const adminListing = info => {
//     return catService.totalCat(info).count().then(total => {
//         this.total = total
//         let list = catService.adminListing(info);
//         if (info.srt_dir && !!info.srt_val)
//             // if (srt_val == 'name')
//             list.sort({ name: srt_dir })
//         return list
//     }).then(async cat => {
//         cat = JSON.parse(JSON.stringify(cat));
//         for (let i = 0; i < cat.length; i++) {
//             let info = { p_id: cat[i]._id }
//             cat[i].subcat = await catService.totalCat(info).count().then(res => res);
//         }
//         let bp = cnfg.s3UploadPaths.endPoint + cnfg.s3UploadPaths.cat.img
//         return { total: this.total, bp, cat }
//     }).catch(err => {
//         throw err
//     })
// }


// const adminListing = info => {
//     return catService.totalCat(info).count().then(total => {
//         this.total = total
//         let list = catService.adminListing(info);
//         if (info.srt_dir && !!info.srt_val)
//             // if (srt_val == 'name')
//             list.sort({ name: srt_dir })
//         return list
//     }).then(async cat => {
//         cat = JSON.parse(JSON.stringify(cat));
//         for (let i = 0; i < cat.length; i++) {
//             info.cat_id = cat[i]._id;
//             cat[i].subcat = await catService.allSubcat(info).then(result => result);
//         }

//         let bp = cnfg.s3UploadPaths.endPoint + cnfg.s3UploadPaths.cat.img
//         return { total: this.total, bp, cat }
//     }).catch(err => {
//         throw err
//     })
// }
const adminListing = info => {

    return catService.adminListing(info).then(cat => {
        return { 'responseMessage': "Get List successfully", 'result': cat[0].catData ? cat[0].catData : [], 'totalRecord': cat[0].total[0] ? cat[0].total[0].count : 0 };
    }).catch(err => {
        throw err
    })
}
const deleteCat = info => {
    return catService.deleteCat(info).then(delcat => {
        if (!delcat.is_del) {
            info.p_id = delcat._id
            catService.deleteChild(info).then(delchild => {
                if (delchild.nModified) {
                    info.p_id = delchild._id
                    catService.deleteChild(info).then(delchild => delchild)
                }
            })
            return { ...status_codes.del_cat }
        }
        throw customExc.completeCustomException('alrdy_del_cat')
    }).catch(err => {
        throw err
    })
}

const editCat = info => {
    return catService.editCat(info).then(upCat => {
        if (upCat) return { ...status_codes.cat_up_succ }
    }).catch(err => {
        throw err;
    })
}


function getCatData(params) {
    if (params.p_id != null) {
        return catService.getCatdetails({ '_id': params.p_id })
            .then(res => {
                params.parent_cat.push({ 'p_id': res._id, 'name': res.name });
                res = _.extend(res, { 'parent_cat': params.parent_cat });
                return getCatData(res);

            })
    } else {

        return new Promise(function (resolve, reject) {
            return resolve(params);
        });
    }
}


// const cat= async info=>{
//     var res=await __cat(info);
// }

// const __cat=async info=>{
// var data= await cat
// }



//========================== Export Module Start ==============================

module.exports = {
    addCat,
    getCat,
    dropdownList,
    adminListing,
    deleteCat,
    editCat
};

//========================== Export Module End ===============================signUp