"use strict";

//========================== Load Modules Start =======================
const mongoose = require('mongoose');
//========================== Load internal modules ====================
const cnst = require('./../../constant');
const catDao = require('./catDao');
const { regexIncase } = require('./../../appUtils')

//========================== Load Modules End ==============================================


const addCat = info => {
    console.log(info, "###");
    delete info['_id'];
    return catDao.insert(info);
}

const getCat = info => {
    let query = { p_id: info.p_id };
    if (!!info.srch) {
        query.name = regexIncase(info.srch);
    }
    // if (!!info.p_id) {
    //     query.p_id = info.p_id
    // }
    let project = { name: 1, p_id: 1 }
    console.log(query, project, "##################");
    return catDao.getMany(query, project);
}

const dropdownList = info => {
    // let query = { p_id: info.p_id }, project = { name: 1, ancestors: 1 };
    let query = {}, project = { name: 1, ancestors: 1, level: 1 };
    switch (info.ref) {
        case "product":
            query.p_id = { $ne: null }
    }
    if (!!info.srch)
        query.name = regexIncase(info.srch)
    return catDao.getMany(query, project)
}
const allSubcat = info => {
    let parent_cat = info.cat_id;
    let query = { ancestors: { $in: parent_cat } }, project = { name: 1, img: 1, level: 1 };
    return catDao.getMany(query, project);
}
// const adminListing = info => {
//     let query = __listing_filter(info),
//         limit = 5, skip = 0, project = { name: 1, p_id: 1, desc: 1, attr: 1, is_del: 1, img: 1 };
//     if (info.limit && !isNaN(info.limit))
//         limit = Number(info.limit);
//     if (info.page && !isNaN(info.page))
//         skip = Number(limit * info.page)
//     let paginate = { limit, skip };
//     return catDao.getMany(query, project, paginate)
// }
// const totalCat = info => {
//     let query = __listing_filter(info);
//     return catDao.getMany(query)
// }
const adminListing = info => {

    let agePipe = [];
    let match = {};
    agePipe.push({
        $project:
        {
            _id: 1, name: 1,
            parent_cat: 1,
            view_name: 1,
            // p_size: {$size: "$parent_cat"}
        }
    });
    if (info.srch) {
        agePipe.push({ '$match': { name: { '$regex': info.srch, '$options': '-i' } } });
    }
    agePipe.push({
        '$sort':
        {
            'name': 1,
            // 'p_size':1
        }
    });
    let skip = parseInt((info.page - 1) * info.limit);
    agePipe.push({
        '$facet': {
            catData: [
                { '$skip': parseInt(skip) },
                { '$limit': parseInt(info.limit) }
            ],
            total: [
                { '$group': { _id: null, count: { '$sum': 1 } } }
            ]
        }
    });

    return catDao.agg(agePipe);

    // let pipe = [
    //     { $unwind: { path: "$ancestors", preserveNullAndEmptyArrays: true } },
    //     {
    //         $lookup: {
    //             from: "categories",
    //             localField: "_id",
    //             foreignField: "ancestors",
    //             as: "subcat"
    //         }
    //     },
    //     // { $unwind: { path: "$subcat", preserveNullAndEmptyArrays: true } },
    //     // {
    //     //     $group: {
    //     //         _id: "$_id",
    //     //         name: { $first: "$name" },
    //     //         anctrs: { $push: "$ancestors" },
    //     //         new_name: { $addToSet: { $concat: ["$subcat.name", ">"] } }
    //     //     }
    //     // },
    //     // {
    //     //     $match: {
    //     //         anctrs: { $eq: [] }
    //     //     }
    //     // }
    // ];

}
const deleteCat = info => {
    let query = { _id: info.cat_id }, update = { is_del: true }
    return catDao.updateById(query, update);
}
const deleteChild = info => {
    if (!!info.p_id) {
        let query = { p_id: info.p_id }, update = { is_del: true };
        return catDao.updateMany(query, update);
    } else
        return false
}


const getCatdetails = params => {
    if (params._id != null) {
        return catDao.getByKey(params);
    }
}



const editCat = info => {
    let query = { _id: info.cat_id }, update = info.update;
    return catDao.updateById(query, update);
}
const __listing_filter = info => {
    let query = { p_id: info.p_id }
    if (!!info.srch)
        query.name = regexIncase(info.srch)
    return query;
}
// const dropdownList = info => {
//     let pipe = [
//         {
//             $lookup: {
//                 from: "categories",
//                 localField: "_id",
//                 foreignField: "p_id",
//                 as: "child"
//             }
//         },
//         {
//             $match: { $or: [{ child: { $ne: [] } }, { p_id: { $eq: null } }] }
//         },
//         { $project: { name: 1 } }
//     ];
//     if (!!info.srch) {
//         let obj = { $match: { name: regexIncase(info.srch) } };
//         pipe.unshift(obj)
//     }
//     return catDao.agg(pipe);
// }







//========================== Export Module Start ==============================

module.exports = {
    addCat,
    getCat,
    dropdownList,
    adminListing,
    // totalCat,
    deleteCat,
    deleteChild,
    allSubcat,
    editCat,
    getCatdetails
};

//========================== Export Module End ===============================