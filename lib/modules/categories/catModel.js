// Importing mongoose
var mongoose = require("mongoose");
var constants = require("../../constant");

/*var rootRef = firebase.database();*/

var Schema = mongoose.Schema;

var CatSchema = new Schema({
    name: {
        type: String,
        index: true,
        required: true,
        lowercase: true
    },
    view_name:{
        type: String,
        index:true,
        required:true
    },
    parent_cat:[{
      p_id:{type:Schema.ObjectId},
      name: {type: String},
      _id: 0
     }],
    p_id: {
        type: Schema.Types.ObjectId,
        index: true,
        default: null
    },
    img: String,
    url: String,
    ancestors: [{ type: Schema.Types.ObjectId, index: true }],
    meta_title: String,
    desc: String,
    attr: String,
    order: String,
    tag: String,
    level: { type: Number, default: 0, enum: [0, 1, 2] },//0=>parent,1=>child,2=>grandChild
    is_del: {
        type: Boolean,
        default: false,
    },
    status: {
        type: Boolean,
        default: false
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

//Export user module
module.exports = mongoose.model(constants.DB_MODEL_REF.CAT, CatSchema);
/*module.exports = rootRef*/