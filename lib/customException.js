//========================== Load Modules Start ===========================

//========================== Load Internal Module =========================

// Load exceptions
var Exception = require("./model/Exception");
var status_codes = require("./status_codes.json");

//========================== Load Modules End =============================

//========================== Export Module Start ===========================

module.exports = {
    intrnlSrvrErr: err => {
        console.log(err, 'now err');
        return new Exception(
            status_codes.intrnlSrvrErr.code,
            status_codes.intrnlSrvrErr.msg,
            err
        )
    },
    unauthorizeAccess: (err = null) => {
        if (!err)
            return new Exception(
                status_codes.unauth_access_id.code,
                status_codes.unauth_access_id.msg
            );
        else
            return new Exception(
                status_codes.unauth_access.code,
                status_codes.unauth_access.msg,
                err
            );
    },
    tokenGenException: err =>
        new Exception(
            status_codes.tokenGenError.code,
            status_codes.tokenGenError.msg,
            err
        ),
    getCustomErrorException: (errMsg, error) => new Exception(5, errMsg, error),
    validationErrors: err => {
        // console.log('validationErrors', err, status_codes.valid_errors);
        return new Exception(
            status_codes.valid_errors.code,
            status_codes.valid_errors.msg,
            err
        )
    },
    dbErr: err =>
        new Exception(
            status_codes.intrnlSrvrErr.code,
            status_codes.intrnlSrvrErr.msg,
            err
        ),
    completeCustomException: (type, error = false) => {
        // console.log(type, 'errcode', error , 'errMsg, error');

        if (!error)
            return new Exception(status_codes[type].code, status_codes[type].msg);
        else
            return new Exception(
                status_codes[type].code,
                status_codes[type].msg,
                error
            );
    },
    customExcWithData: (type, data) =>
        // console.log(type, 'customExcWithData', 'data', data);
        new Exception2(status_codes[type].code, status_codes[type].msg, data)
};

//========================== Export Module   End ===========================