const express = require("express"),
    bodyParser = require("body-parser"), // parses information from POST
    methodOverride = require("method-override"); // used to manipulate POST
var auth = require("basic-auth");

module.exports = function (app, env) {
    // parses application/json bodies
    app.use(bodyParser.json());
    // parses application/x-www-form-urlencoded bodies
    // use queryString lib to parse urlencoded bodies
    app.use(
        bodyParser.urlencoded({
            extended: false
        })
    );
    app.use(
        methodOverride(function (request, response) {
            if (
                request.body &&
                typeof request.body === "object" &&
                "_method" in request.body
            ) {
                // look in urlencoded POST bodies and delete it
                var method = request.body._method;
                delete request.body._method;
                return method;
            }
        })
    );

    /**
     * add swagger to our project
     */
    app.use("/apiDocs", express.static(app.locals.rootDir + "/public/dist"));
    app.use("/images", express.static(app.locals.rootDir + "/public/images"));
    app.use("/pages", express.static(app.locals.rootDir + "/public/view"));
    // app.use("/mail", express.static(app.locals.rootDir + "/public/mail_verify"));
    app.use("/chatTest", express.static(app.locals.rootDir + "/views"));
    /*
     * all api request
     */
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Credentials", true);
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, authorization, accessToken"
        );
        res.setHeader(
            "Access-Control-Allow-Methods",
            "POST, GET, PUT, DELETE, OPTIONS"
        );
        if (req.method == "OPTIONS") {
            res.status(200).end();
        } else {
            next();
        }
    });
};