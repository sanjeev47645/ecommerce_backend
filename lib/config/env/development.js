module.exports = {
    environment: 'development',
    ip: '',
    port: 6030,
    isDev: true,
    mongo: {
        dbName: 'ecomdev',
        dbUrl: `mongodb://localhost:28039/`,
        options: {
            user: "ecomdevUser",
            pass: "ecomDev@12345#1234",
            useNewUrlParser: true,
            useCreateIndex: true,
        },
    },

    redis: {
        server: 'localhost',
        port: 6379,
        auth_pass: "ecomDev@123#123"
    },
    s3UploadPaths: {
        endPoint: "https://ecomerceimg.s3.ap-south-1.amazonaws.com/",
        user: {
            profile: "ecom_dev/user/profile/"
        },
        cat: {
            img: "ecom_dev/cat/img/"
        },
        product: {
            img: "ecom_dev/product/img/"
        }
    },
    settings: {
        ID: '5d2319667af08c29e5443820'
    }
}