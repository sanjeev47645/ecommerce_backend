module.exports = {
    environment: 'production',
    ip: '',
    port: 5003,
    isProd: true,
    mongo: {
        dbName: 'ecom',
        dbUrl: 'mongodb://localhost:29010/',
        options: {
            user: 'ecom',
            pass: 'ecom'
        },
    },
    s3UploadPaths: {
        endPoint: "https://ecomerceimg.s3.ap-south-1.amazonaws.com/",
        user: {
            profile: "ecom_prod/user/profile/"
        },
        cat: {
            img: "ecom_prod/cat/img/"
        },
        product: {
            img: "ecom_prod/product/img/"
        }
    },
    settings: {
        ID: '5d2319667af08c29e5443820'
    }
}