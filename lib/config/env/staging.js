module.exports = {
    environment: 'staging',
    ip: '',
    port: 3010,
    isDev: true,
    mongo: {
        dbName: 'ecomStag',
        dbUrl: `mongodb://localhost:29030/`,
        options: {
            user: "ecom",
            pass: "!@#$%ecom^&*(",
            useNewUrlParser: true,
            useCreateIndex: true,
        }
    },

    redis: {
        server: 'localhost',
        port: 6379,
        auth_pass: "EcomMerce"
    },
    s3UploadPaths: {
        endPoint: "https://ecomerceimg.s3.ap-south-1.amazonaws.com/",
        user: {
            profile: "ecom_remote/user/profile/"
        },
        cat: {
            img: "ecom_stag/cat/img/"
        },
        product: {
            img: "ecom_stag/product/img/"
        }
    }

}