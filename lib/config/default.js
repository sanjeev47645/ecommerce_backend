const path = require('path');
module.exports = {
    environment: 'local',
    ip: 'local',
    port: 3090,
    uploadDir: path.resolve('./uploads'),
    mongo: {
        dbName: 'ecomlocal',
        dbUrl: "mongodb://localhost:27017/",
        options: {
            user: "ecom",
            pass: "ecom!#"
            // server: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}},
            // replset: {socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}}
        }
    },
    isDev: true,

    //the above keys are used when someone used othan required envionments
    redis: {
        server: 'localhost',
        port: 6379
    },

    BASE_URL: {
        URL: 'http://13.235.128.238:3010',
        //PORT: '3010'
    },

    TOKEN_EXPIRATION_SEC: 60 * 24 * 60 * 60,
    JWT_SECRET_KEY: 'g8b9(-=~Sdf)',

    twilio: {
        ACCOUNT_SID: 'AC00924010c2a9334d2a3e8edcee5cf2e0',
        AUTH_TOKEN: 'a49d067b0ba6c9cef352e5526593383c',
        TWILIO_NUMBER: '+14243222424',
        AUTHY_API_KEY: ""
    },

    cloudFront: {
        publicDomain: "",
        domain: '',
        keyPair: "",
        // privateKeyPath : path.resolve( __dirname , '../../resources/private.pem')
    },

    awsIamUser: {
        accessKeyId: "AKIAXVOADCXS2ACCWFHF",
        secretAccessKey: "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",
    },

    s3: {
        maxAsyncS3: 0, // this is the default
        s3RetryCount: 0, // this is the default
        s3RetryDelay: 0, // this is the default
        multipartUploadThreshold: 20975465, // this is the default (20 MB)
        multipartUploadSize: 15728640, // this is the default (15 MB)
        bucketName: "ecomerceimg",
        publicBucketName: "",
        // signatureVersion: 'v2',
        region: "us-east-1",
        //region: "Asia Pacific(Mumbai)",
        ACL: 'public-read'
    },
    smtp: {
        host: 'Gmail',
        auth: {
            user: 'trueparkingapp@gmail.com',
            pass: 'truepark@123#'
        }
    },

    basicAuth: {
        "username": "ecomUser",
        "password": "(*@!@ecom123$%&^"
    },
    path: {
        frgt_pss_admn: "http://13.233.130.69/reset-password/",
        verify_mail: "http://13.235.128.238"
    },
    url: {
        basePath: ""
    },
    website: {
        basePath: "",
        forgotPassword: {}
    },
    paymentMethods: {
        stripe: {
            publishableKey: '',
            secretKey: '',
            appleMerchendId: '',
            country: {
                name: 'Australia',
                code: 'AU',
                currency: {
                    symbol: '$',
                    abbreviationOf: 'British Pound',
                    abbreviation: 'AUD',
                    isoCode: 'aud'
                }
            },
            connectAccountType: 'custom',
            accountType: 'individual',
            charge: {
                lessonDescription: 'For lesson completion.'
            },
            bankAccountType: {
                bankAccount: 'bank_account',
                card: 'card'
            }
        }
    },

    sightenEngine: {
        user: "",
        secret: ""
    },

    //Firebase
    firebase: {
        //Cloud messaging
        fcm: {
            serverKey: 'AAAAWosjWtk:APA91bHQgCdpWl8clH4tVodP-OxBrcmZ64zMvOf6QyZnR1gmR2E5NNaX5oqRDAhIUdy555PfiK9UCNk6s9vV_IapujhxEKGyvs5_UnSriaeZIIA1G-obKm2iCkIpQCU6gdBn6Qssow3p',
            legacyServerKey: '',
            senderId: '',
        },
        subscribeTopics: {},
        // adminServiceAccount: require('./JSON FILE')
    }

}