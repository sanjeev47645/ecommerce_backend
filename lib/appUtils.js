"use strict";

//========================== Load Modules Start ===========================

//========================== Load External Module =========================

var sha256 = require('sha256');
var randomstring = require("randomstring");
const moment = require('moment');
const bcrypt = require('bcryptjs')
//========================== Load Modules End =============================

//========================== Export Module Start ===========================


/**
 * return user home
 * @returns {*}
 */
function getUserHome() {
    return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
}

function getNodeEnv() {
    return process.env.NODE_ENV;
}

/**
 * returns if email is valid or not
 * @returns {boolean}
 */
function isValidEmail(email) {
    var pattern = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
    return new RegExp(pattern).test(email);
}

/**
 * returns if zipCode is valid or not (for US only)
 * @returns {boolean}
 */
function isValidPhoneNumber(num) {
    if (Number.isInteger(num)) {
        num = num.toString();
    }
    if (phoneNumber.indexOf("+") > -1)
        return new RegExp(pattern).test(zipcode);
}
/**
 * returns if zipCode is valid or not (for US only)
 * @returns {boolean}
 */
function isValidZipCode(zipcode) {
    var pattern = /^\d{5}(-\d{4})?$/;
    return new RegExp(pattern).test(zipcode);
}
/**
 * returns if zipCode is valid or not (for US only)
 * @returns {boolean}
 */
function createHashSHA256(pass) {
    return sha256(pass)
}

/**
 * returns random number for password
 * @returns {string}
 */
var getRandomPassword = function () {
    return getSHA256(Math.floor((Math.random() * 1000000000000) + 1));
};

var getSHA256 = function (val) {
    return sha256(val + "password");
};

const encryptHashPassword = (password, salt) => bcrypt.hashSync(password, salt)

const generateSaltAndHashForPassword = password => {
    var encryptPassword = {
        salt: "",
        hash: ""
    };
    encryptPassword['salt'] = bcrypt.genSaltSync(10);
    encryptPassword['hash'] = bcrypt.hashSync(password, encryptPassword['salt']);
    return encryptPassword;
}




/**
 *
 * @param string
 * @returns {boolean}
 */
var stringToBoolean = function (string) {
    switch (string.toLowerCase().trim()) {
        case "true":
        case "yes":
        case "1":
            return true;
        case "false":
        case "no":
        case "0":
        case null:
            return false;
        default:
            return Boolean(string);
    }
}
/**
 *
 * @returns {string}
 * get random 6 digit number
 * FIX ME: remove hard codeing
 * @private
 */
function getRandomOtp() {
    //Generate Random Number
    return randomstring.generate({
        charset: 'numeric',
        length: 6
    });
}

function isValidPhone(phone, verifyCountryCode) {
    var reExp = verifyCountryCode ? /^\+\d{6,16}$/ : /^\d{6,16}$/;
    return reExp.test(phone)
}

function createRedisValueObject(obj) {
    var respObj = {};
    var user = obj.user;
    respObj.userId = user._id;
    respObj.isAdmin = user.isAdmin;
    return respObj;
}
const genOtp = () => Math.floor(1000 + Math.random() * 9000);

var MToKm = () => 0.001;

var MtoM = () => 0.001 * 0.621371;


const isObjEmp = (obj) => {
    return Object.keys(obj).length > 0 ? false : true;
}

const genRandStr = digit => {
    let num = Number(digit)
    if (num) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < num; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    } else {
        return '';
    }
}

const base64En = str => {
    return Buffer.from(str).toString('base64');
}

const base64De = Enstr => {
    return Buffer.from(Enstr, 'base64').toString('ascii')
}

const withinDay = date => {
    let tommorow = moment(),
        today = moment(date),
        hours = tommorow.diff(today, 'hours')
    return hours < 24 ? true : false;
}

const regexIncase = val => {
    let reg = new RegExp(`${val}`);
    let regObj = {
        $regex: reg,
        $options: "i"
    };
    return regObj;
}

const otp_msg = (flag, otp) => {
    switch (flag) {
        case 'login':
            return `${otp}`
        case 'forgot':
            return `${otp}`
        case 'update':
            return `${otp}`
        default:
            return false;
    }
}

const spl_vhl_no = str => {
    var val = '';
    for (let i = 0; i < str.length; i++) {
        if (i == 2 || i == 4 || i == 6) {
            val += " " + str[i]
        } else {
            val += str[i]
        }
    }
    // if (str.length > 3) {
    //     let val = str.substring(0, 2) + ' ' + str.substring(2, 4) + ' ' + str.substring(4, str.length)
    //     return val;
    // }
    return val;
}

//========================== Export Module Start ===========================

module.exports = {
    appHttpClient: '',
    getUserHome,
    getNodeEnv,
    isValidEmail,
    isValidZipCode,
    isValidPhoneNumber,
    createHashSHA256,
    getRandomPassword,
    encryptHashPassword,
    generateSaltAndHashForPassword,
    stringToBoolean,
    getRandomOtp,
    isValidPhone,
    createRedisValueObject,
    isObjEmp,
    MToKm,
    MtoM,
    genOtp,
    genRandStr,
    base64En,
    base64De,
    withinDay,
    regexIncase,
    spl_vhl_no

};

//========================== Export Module End===========================