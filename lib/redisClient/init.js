const Promise = require('bluebird'),
    redis = require('redis');

Promise.promisifyAll(redis.RedisClient.prototype),
    Promise.promisifyAll(redis.Multi.prototype);

var client;
var config = require('../config').cfg,
    logger = require('../logger');

var init = () => {
    client = redis.createClient({
        ...config.redis
    });
    return client.onAsync('error').then(error => {
        logger.info({
            error
        });
    });
}
exports.setValue = (key, value) => {
    return client.setAsync(key, value).then(response => {
        if (response) {
            logger.info({
                'value': response
            }, '_redisSetValue');
            return response;
        }
    }).catch(error => error);
}
exports.getValue = key => {
    return client.getAsync(key).then(response => response
    ).catch(error => error);
}

exports.expire = (key, expiryTime) => {
    return client.expireAsync(key, expiryTime).then(response => response
        // logger.info({
        //     expire: response
        // }, '_expireToken');
    ).catch(error => {
        logger.error({
            'error': error
        }, '_expireToken');
    });
}

exports.deleteValue = key => {
    return client.delAsync(key).then(response => response
    ).catch(error => {
        throw error
    });
}

init();