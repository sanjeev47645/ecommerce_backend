var Promise = require("bluebird"),
    jwt = Promise.promisifyAll(require("jsonwebtoken")),
    customException = require("./customException"),
    config = require("./config").cfg,
    redisClient = require("./redisClient/init"),
    usrService = require('./modules/user/userService');


const generateAdminToken = adminObject => {
    var options = {};
    return jwt
        .signAsync(adminObject, config.JWT_SECRET_KEY, options)
        .then(jwtToken => jwtToken)
        .catch(error => {
            throw customException.tokenGenException(error);
        });
};

const generateUserToken = userObject => {
    var options = {};
    return jwt
        .signAsync(userObject, config.JWT_SECRET_KEY, options)
        .then(jwtToken => {
            return jwtToken;
        })
        .catch(error => {
            throw customException.tokenGenException(error);
        });
};
const verifyToken = accessToken => {
    return jwt.verifyAsync(accessToken, config.JWT_SECRET_KEY, {})
        .then(tokenPayload => {
            this.tokenPayload = tokenPayload;
            return redisClient.getValue(accessToken);
        })
        .then(reply => {
            console.log(reply);
            
            if (reply)
                return this.tokenPayload;
        })
        .catch(err => {
            throw err
        });
}

var verifyUsrForgotPassToken = acsTokn => {
    return jwt
        .verifyAsync(acsTokn, config.JWT_SECRET_KEY)
        .then(tokenPayload => tokenPayload
        )
        .catch(err => {
            throw customException.unauthorizeAccess(err);
        });
};

var expireToken = function (reqest) {
    var accessToken = reqest.get("accessToken");
    if (accessToken) {
        //blacklist token in redis db
        //it will be removed after 6 months
        redisClient.setValue(accessToken, true);
        redisClient.expire(accessToken, config.TOKEN_EXPIRATION_SEC);
    }
};

// ========================== Export Module Start ==========================
module.exports = {
    generateAdminToken,
    generateUserToken,
    verifyToken,
    expireToken,
    verifyUsrForgotPassToken,
};
// ========================== Export Module End ============================