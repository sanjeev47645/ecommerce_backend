# Node js Structure

Nodejs project structure

## Install

###Install current stable version of nodejs

curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh

sudo bash nodesource_setup.sh

sudo apt-get install nodejs

Nodejs current version check

nodejs -v

###Install redis server


wget http://download.redis.io/redis-stable.tar.gz

tar xvzf redis-stable.tar.gz

cd redis-stable
sudo apt-get install make
sudo apt-get install gcc
sudo apt-get install tcl
sudo apt-get install build-essential
sudo apt-get update
sudo apt install redis-server
sudo cp /home/ubuntu/redis-stable/redis.conf /etc/redis
sudo nano /etc/redis/redis.conf --> change supervised no to supervised systemd
sudo nano /etc/systemd/system/redis.service ->  no changes as already it have
sudo systemctl start redis -> Start redis
sudo systemctl status redis -> Check redis status
Then run redis-cli for the terminal output

For password sudo nano /etc/redis/redis.conf, seacrh requirepass and add password accordingly and the restart the server

sudo service redis restart

now connect redis-cli

Type AUTH redis-password
and then type Ping output will be PONG

https://askubuntu.com/questions/868848/how-to-install-redis-on-ubuntu-16-04
https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04

###Install mongodb

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list

sudo apt-get update

sudo apt-get install -y mongodb-org

sudo service mongod start

sudo service mongod stop

https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## Env setup
create .env file in root folder

add this line in .env file for env set

NODE_ENV="local"


##Redis server
it is use for token store

default redis server setup

server: 'localhost',

port: 6379, (default redis port)

#Ecommerce Db
*** 13.233.130.69 -> Development Database
* Admin User
use admin
db.createUser(
   {
     user: "trueParkingAdmin",
     pwd: "#@truEParkings8#76%",
     roles: ["root"],
   }
)
* Qualify Dev user
use trueparkingdev
db.createUser(
   {
     user: "ecomdevUser",
     pwd: "ecomDev@12345#1234",
      roles: [ {role:"readWrite", db:"ecomdev"} ]
   }
)

#apidoc
sudo npm install apidoc -g

#swagger
download dist folder from github
